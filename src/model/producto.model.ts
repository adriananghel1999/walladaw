export interface Producto{
    id:number;
    name:string;
    price:number;
    description:string;
    category:number;
    /*
    0 tech
    1 cars
    2 home
    */
    image:string;
    pickup:number;
    /*
    0 courier
    1 hand
    */
    userid:number;
    
}
