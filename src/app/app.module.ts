import { SearchproductPage } from './../pages/searchproduct/searchproduct';
import { SettingsPage } from './../pages/settings/settings';
import { LoginPage } from './../pages/login/login';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { PhotoLibrary } from '@ionic-native/photo-library/ngx';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { RegisterPage } from '../pages/register/register';
import { AddproductPage } from '../pages/addproduct/addproduct';
import { EditproductPage } from '../pages/editproduct/editproduct';
import { EmailComposer } from '@ionic-native/email-composer/ngx';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SettingsPage,
    LoginPage,
    AddproductPage,
    EditproductPage,
    SearchproductPage,
    RegisterPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SettingsPage,
    LoginPage,
    AddproductPage,
    EditproductPage,
    SearchproductPage,
    RegisterPage
  ],
  providers: [
    EmailComposer,
    PhotoLibrary,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
