export const PRODUCTOS = [
    {
        id: 0,
        name: "Iphone 6",
        price: 400,
        description: "Vendo este iphone porque no me mola y quiero pillarme el nuevo.",
        category: 0,
        image: "iphone.jpeg",
        pickup: 1,
        userid: 0
    }, {
        id: 1,
        name: "Tablet",
        price: 100,
        description: "La tablet vieja de mi hijo que ahora parece que ya no la usa.",
        category: 0,
        image: "tablet.jpg",
        pickup: 0,
        userid: 1
    },
    {
        id: 2,
        name: "Servidor viejo",
        price: 250,
        description: "Me he comprado un nuevo servidor y ya no necesito este.",
        category: 0,
        image: "server.jpg",
        pickup: 0,
        userid: 0
    },
    {
        id: 3,
        name: "Seat ibiza 2004",
        price: 4000,
        description: "Un viejo seat ibiza del 2004 que esta estropeado.",
        category: 1,
        image: "seatviejo.jpg",
        pickup: 1,
        userid: 2
    },
    {
        id: 4,
        name: "Ford fiesta",
        price: 5000,
        description: "Vendo el viejo ford fiesta de mi abuelo y ya no lo necesita.",
        category: 1,
        image: "fordfiesta.jpeg",
        pickup: 1,
        userid: 2
    },
    {
        id: 5,
        name: "Fiat 600",
        price: 4000,
        description: "Precio negociable.",
        category: 1,
        image: "fiat600.jpg",
        pickup: 1,
        userid: 1
    },
    {
        id: 6,
        name: "Lampara vieja",
        price: 25,
        description: "Vieja lampara que aun funciona.",
        category: 2,
        image: "lampara.jpeg",
        pickup: 0,
        userid: 1
    },
    {
        id: 7,
        name: "Batidora",
        price: 45,
        description: "Batidora philips usada, aun funciona como cuando la compre.",
        category: 2,
        image: "batidora.jpeg",
        pickup: 1,
        userid: 0
    },
    {
        id: 8,
        name: "Abono",
        price: 15,
        description: "Perfecto para las plantas.",
        category: 2,
        image: "abono.jpeg",
        pickup: 1,
        userid: 2
    },
    {
        id: 9,
        name: "Radio portatil",
        price: 25,
        description: "Una radio de bolsillo en perfecto estado.",
        category: 0,
        image: "radioportatil.jpeg",
        pickup: 1,
        userid: 0
    },
    {
        id: 10,
        name: "Comoda usada",
        price: 50,
        description: "Comoda algo vieja pero ya no la necesito y me da pena tirarla.",
        category: 2,
        image: "comoda.jpeg",
        pickup: 1,
        userid: 1
    },
]