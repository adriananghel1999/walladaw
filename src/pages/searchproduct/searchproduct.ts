import { PRODUCTOS } from './../../data/producto.data';
import { Usuario } from './../../model/usuario.model';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { USUARIOS } from './../../data/usuario.data';
import { Producto } from '../../model/producto.model';

/**
 * Generated class for the SearchproductPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-searchproduct',
  templateUrl: 'searchproduct.html',
})
export class SearchproductPage {

  category: number;
  range: number = 0;
  usuario: string;

  USUARIOS: Usuario[];

  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events) {
    this.initializeItems();

  }

  filter() {
    let arrayTempora: Producto[];
    arrayTempora = [];
    for (let producto of PRODUCTOS) {
      let category: boolean = false;
      let range: boolean = false;
      let product: boolean = false;

      if (producto.category == this.category) {
        category = true;

      }

      if (this.range <= producto.price) {
        range = true;
      }

      for (let user of USUARIOS){
        if(user.name == this.usuario && producto.userid == user.id){
          product = true;
        }
      }

      if (category && range && product || category && range || product && range) {
        arrayTempora.push(producto);
      } else if (range && this.category == undefined && this.category == undefined){
        arrayTempora.push(producto);
      }

      console.log(category + " - " + range + " - " + product);

    }

    this.events.publish('arrayFiltro', arrayTempora);

    this.navCtrl.pop();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchproductPage');
  }

  initializeItems() {
    this.USUARIOS = USUARIOS.slice(0);
  }

  getItems(ev: any) {
    this.initializeItems();

    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.USUARIOS = this.USUARIOS.filter((usuario) => {
        return (usuario.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }


}




