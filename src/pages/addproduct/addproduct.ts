import { PRODUCTOS } from './../../data/producto.data';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Events } from 'ionic-angular';
import { PhotoLibrary } from '@ionic-native/photo-library/ngx';

/**
 * Generated class for the AddproductPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-addproduct',
  templateUrl: 'addproduct.html',
})
export class AddproductPage {

  name: string = "";
  price: number = 0;
  category: number = 0;
  pickup: number = 0;
  description: string = "";
  image:string = "";
  userid:number = this.navParams.get("userid");

  constructor(public navCtrl: NavController, public events: Events, public navParams: NavParams, public alertCtrl: AlertController, private photoLibrary: PhotoLibrary) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddproductPage');
  }

  add() {

    if(this.name == "" || this.description == ""){
      const alert = this.alertCtrl.create({
        title: 'Empty spaces!',
        subTitle: "You should complete all the fields",
        buttons: ['OK']
      });
      alert.present();
    } else {

    if (this.category == 0){
      this.image = "tecno.jpg";
    } else if (this.category == 1){
      this.image = "coche.jpg";
    } else if (this.category == 2){
      this.image = "casa.jpeg";
    }

    PRODUCTOS.push({
      id: PRODUCTOS.length,
      name: this.name,
      price: this.price,
      description: this.description,
      category: this.category,
      /*
      0 tech
      1 cars
      2 home
      */
      image: this.image,
      pickup: this.pickup,
      /*
      0 courier
      1 hand
      */
      userid: this.userid
    });

    this.events.publish('vuelvoDeAdd', PRODUCTOS);
    this.navCtrl.pop();}
  }

  addImage() {
    this.photoLibrary.requestAuthorization().then(() => {
      this.photoLibrary.getLibrary().subscribe({
        next: library => {
          library.forEach(function (libraryItem) {
            console.log(libraryItem.id);          // ID of the photo
            console.log(libraryItem.photoURL);    // Cross-platform access to photo
            console.log(libraryItem.thumbnailURL);// Cross-platform access to thumbnail
            console.log(libraryItem.fileName);
            console.log(libraryItem.width);
            console.log(libraryItem.height);
            console.log(libraryItem.creationDate);
            console.log(libraryItem.latitude);
            console.log(libraryItem.longitude);
            console.log(libraryItem.albumIds);    // array of ids of appropriate AlbumItem, only of includeAlbumsData was used
          });
        },
        error: err => { console.log('could not get photos'); },
        complete: () => { console.log('done getting photos'); }
      });
    })
      .catch(err => console.log('permissions weren\'t granted'));
  }

}
