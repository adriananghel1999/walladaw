import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { USUARIOS } from '../../data/usuario.data';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  userR: string = "";
  emailR: string = "";
  passwordR: string = "";

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController) {
  }

  // register
  register() {
    if (this.userR == "" || this.emailR == "" || this.passwordR == "") {

      const alert = this.alertCtrl.create({
        title: 'Empty spaces!',
        subTitle: "You should complete all the fields",
        buttons: ['OK']
      });
      alert.present();

    } else {
      USUARIOS.push({
        id: USUARIOS.length,
        name: this.userR,
        email: this.emailR,
        password: this.passwordR,
        preference: 0
      });

      this.navCtrl.setRoot(LoginPage);
    }

  }

  // go to login page
  login() {
    this.navCtrl.setRoot(LoginPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

}
