import { USUARIOS } from './../../data/usuario.data';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, AlertController } from 'ionic-angular';

/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  email:string;
  name:string;
  theme:number;
  sessionId: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events,public alertCtrl: AlertController) {
    events.subscribe('sessionId', (data) => {
      this.sessionId = data;

      this.email = USUARIOS[this.sessionId].email;
      this.name = USUARIOS[this.sessionId].name;
      this.theme = USUARIOS[this.sessionId].preference;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');
  }

  ionViewWillEnter() {
    
  }

  saveSettings(){

    if (this.email == "" || this.name == ""){
      const alert = this.alertCtrl.create({
        title: 'Empty spaces!',
        subTitle: "You should complete all the fields",
        buttons: ['OK']
      });
      alert.present();
    } else {


    USUARIOS[this.sessionId].email = this.email;
    USUARIOS[this.sessionId].name = this.name;
    USUARIOS[this.sessionId].preference = this.theme;

    this.navCtrl.pop();}
  }

}
