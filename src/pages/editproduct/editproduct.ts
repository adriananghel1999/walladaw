import { PRODUCTOS } from './../../data/producto.data';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

/**
 * Generated class for the EditproductPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-editproduct',
  templateUrl: 'editproduct.html',
})
export class EditproductPage {

  productPosition: number = this.navParams.get("position");
  productId: number = this.navParams.get("id");

  name: string = PRODUCTOS[this.productPosition].name;
  price: number = PRODUCTOS[this.productPosition].price;
  category: number = PRODUCTOS[this.productPosition].category;
  pickup: number = PRODUCTOS[this.productPosition].pickup;
  description: string = PRODUCTOS[this.productPosition].description;

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditproductPage');
  }

  save() {

    if (this.name == "" || this.description == "") {
      const alert = this.alertCtrl.create({
        title: 'Empty spaces!',
        subTitle: "You should complete all the fields",
        buttons: ['OK']
      });
      alert.present();
    } else {


      PRODUCTOS[this.productPosition].name = this.name;
      PRODUCTOS[this.productPosition].price = this.price;
      PRODUCTOS[this.productPosition].category = this.category;
      PRODUCTOS[this.productPosition].pickup = this.pickup;
      PRODUCTOS[this.productPosition].description = this.description;

      this.navCtrl.pop();
    }
  }

}
