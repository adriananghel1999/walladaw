import { SearchproductPage } from './../searchproduct/searchproduct';
import { AddproductPage } from './../addproduct/addproduct';
import { EditproductPage } from './../editproduct/editproduct';
import { Usuario } from './../../model/usuario.model';
import { USUARIOS } from './../../data/usuario.data';
import { Component } from '@angular/core';
import { NavController, Events } from 'ionic-angular';
import { PRODUCTOS } from './../../data/producto.data';
import { Producto } from '../../model/producto.model';
import { SettingsPage } from '../settings/settings';
import { LoginPage } from '../login/login';
import { EmailComposer } from '@ionic-native/email-composer/ngx';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  pages: Array<{ title: string, component: any }>;
  PRODUCTOS: Producto[];
  USUARIOS: Usuario[];
  sessionId: number;
  textoBusqueda: string;

  constructor(public navCtrl: NavController, public events: Events, private emailComposer: EmailComposer) {
    this.PRODUCTOS = PRODUCTOS.slice(0);
    events.subscribe('sessionId', (data) => {
      this.sessionId = data;
      console.log(this.sessionId);
    });
    events.subscribe('arrayFiltro', (data) => {
      this.PRODUCTOS = data;
      console.log(this.PRODUCTOS);
    });
    events.subscribe('vuelvoDeAdd', (data) => {
      this.PRODUCTOS = data;
    });

    // mis paginas to guapas
    this.pages = [
      { title: 'Settings', component: SettingsPage },
      { title: 'Log out', component: LoginPage }
    ];
  }

  // pushea y tal, si te vas a log out hace un setroot en vez de push, tambien me controla las sesiones
  openPage(page) {
    if (page == LoginPage) {
      this.navCtrl.setRoot(page.component)
    } else {
      this.navCtrl.push(page.component).then(() => {
        this.events.publish('sessionId', this.sessionId);
      });
    }


  }

  // Cambiar el id de usuario a nombre
  useridToName(id) {
    let userString = USUARIOS[id].name;
    return userString;
  }

  // Lo mismo pero con categorias
  categoryidToString(id) {
    if (id == 0) {
      return "Tecnology";
    } else if (id == 1) {
      return "Cars";
    } else if (id == 2) {
      return "Home";
    }
  }

  modfyProduct(index) {
    this.navCtrl.push(EditproductPage, {
      position: index,
      id: this.PRODUCTOS[index].id
    });
  }

  deleteProduct(index) {
    this.PRODUCTOS.splice(index, 1);
    PRODUCTOS.splice(index, 1);
  }

  addProduct() {
    this.navCtrl.push(AddproductPage, {
      userid: this.sessionId
    });
  }

  // Email al pulsar contacto

  contact(index) {

    this.emailComposer.isAvailable().then((available: boolean) => {
      if (available) {

        let email = {
          to: USUARIOS[PRODUCTOS[index].userid].email,
          cc: USUARIOS[this.sessionId].email,
          subject: 'Comprar: ' + PRODUCTOS[index].name,
          body: 'Quiero comprarte este producto.',
          isHtml: true
        }

        this.emailComposer.open(email);
      }
    });



  }

  // Cosas de busqueda

  searchBar() {
    this.PRODUCTOS.length = 0;

    for (let producto of PRODUCTOS) {
      if (producto.name.toLowerCase().includes(this.textoBusqueda.toLowerCase())) {
        this.PRODUCTOS.push(producto);
      }
    }
  }

  search() {
    this.navCtrl.push(SearchproductPage);
  }

}

