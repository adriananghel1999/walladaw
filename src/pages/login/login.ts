import { USUARIOS } from './../../data/usuario.data';
import { RegisterPage } from './../register/register';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, AlertController } from 'ionic-angular';
import { Usuario } from '../../model/usuario.model';
import { HomePage } from '../home/home';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  USUARIOS: Usuario[];

  email: string;
  password: string;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public navParams: NavParams, public events: Events) {
  }

  // go to register page
  register() {
    this.navCtrl.setRoot(RegisterPage);
  }

  login() { // meter validaciones

    if (this.email == "" || this.password == "") {
      const alert = this.alertCtrl.create({
        title: 'Empty spaces!',
        subTitle: "You should complete all the fields",
        buttons: ['OK']
      });
      alert.present();
    } else {


      for (let i in USUARIOS) {
        if (USUARIOS[i].email == this.email && USUARIOS[i].password == this.password) {
          console.log(USUARIOS[i].id);
          this.navCtrl.setRoot(HomePage).then(() => {
            this.events.publish('sessionId', USUARIOS[i].id);
            console.log(USUARIOS[i].id);
          });
          break;
        }
      }

    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

}
